/*! Shareable Socials (shareable.js ) - v1.0.1 - 2016-04-07
 *  Desc: Lightweight social shareable
 *  Requires: 
 *	- Font Awesome
 *    - Manific Popup
 *  Copyright: (c) 2016 Wawan - wawan@potential.asia
 *  License: MIT
 */
 (function(window, $, undefined) {
      $.fn.shareableDefaults  =  {
            template: 'default',
            apiKey : 'a2afeff9892be7a9c12b84f36fce4d8dc6ca6dda',
            providers: {
                  twitter: {
                        name : "twitter",
                        url :"http://twitter.com/share?",
                        icon: "fa fa-twitter"
                  },
                  facebook: {
                        name : "facebook",
                        url : "https://www.facebook.com/sharer/sharer.php?",
                        icon: "fa fa-facebook"
                  },
                  linkedin: {
                        name : "linkedin",
                        url :"http://www.linkedin.com/shareArticle?",
                        icon: "fa fa-linkedin"
                  },
                  googleplus: {
                        name : "googleplus",
                        url :"https://plus.google.com/share?",
                        icon: "fa fa-google-plus"
                  },
                  reddit: {
                        name : "reddit",
                        url : "http://www.reddit.com/submit?",
                        icon: "fa fa-reddit"
                  },
                  email: {
                        name : "email",
                        url : "mailto:?",
                        icon: "fa fa-envelope"
                  }
            },
            defaults: {
                  twitter: {
                        text: null,
                        url: null,
                        via: 'shareable'
                  },
                  facebook: {
                        u: null
                  },
                  linkedin: {
                        mini: 'true',
                        url: null,
                        title: null,
                        summary: null
                  },
                  googleplus: {
                        url: null
                  },
                  reddit: {
                        url: null,
                        title: null
                  },
                  email: {
                        subject: null,
                        body: null
                  }
            },
      };

      function keyExists(key, search) {
            if (!search || (search.constructor !== Array && search.constructor !== Object)) {
                  return false;
            }
            for (var i = 0; i < search.length; i++) {
                  if (search[i] === key) {
                        return true;
                  }
            }
            return key in search;
      }

      function _createSocialButton(network, parent, provider, defaults) {
            shareUrl = provider.url;
            shareUrl += $.param(defaults);
            var button = '<a href="'+shareUrl+'" class="soslink sharelink '+network+'"><i class="'+provider.icon+'"></i><span class="name">'+provider.name+'</span></a>';
            $(parent).append(button);
      } 

      function _createEmailButton(network, parent, provider, defaults) {
            shareUrl = provider.url;
            shareUrl += $.param(defaults);
            var button = '<a href="'+shareUrl+'" class="soslink '+network+'"><i class="'+provider.icon+'"></i><span class="name">'+provider.name+'</span></a>';
            $(parent).append(button);
      } 

      /* use bitly shorterner
      problem cant return anything from ajax. next move to nodejs to handle convertion
      */



      $.fn.shareableSocials = function(options) {
            options = $.extend({}, $.fn.shareableDefaults, options || {});

            var count = 0;
            var defaultUrl = window.location.href;
       
            
            this.each(function() {
                  count++;
                  

                  var elem = $(this);
                  var shareData = [];
                  var url = elem.attr('data-url');
                  var title = elem.attr('data-title');
                  var text = elem.attr('data-desc');
                  var summary = elem.attr('data-desc');
                  var image = elem.attr('data-image');
                  var tweet = elem.attr('data-tweet');
                  var subject = elem.attr('data-subject');
                  var body = elem.attr('data-body');

                

                  if (typeof url !== typeof undefined && url !== false) {
                        shareData['url'] = url;
                        shareData['u'] = url;
                  } else {
                        shareData['url'] = defaultUrl;
                        shareData['u'] = defaultUrl;
                  }
                  if (typeof title !== typeof undefined && title !== false) {
                        shareData['title'] = title;
                  }
                  if (typeof text !== typeof undefined && text !== false) {
                        shareData['text'] = text;
                  }

                  if (typeof tweet !== typeof undefined && tweet !== false) {
                        shareData['text'] = tweet;
                  } else {
                        shareData['text'] = text;
                  }

                  if (typeof subject !== typeof undefined && subject !== false) {
                        shareData['subject'] = subject;
                  }

                  if (typeof body !== typeof undefined && body !== false) {
                        shareData['body'] = body;
                  }

                  if (typeof summary !== typeof undefined && summary !== false) {
                        shareData['summary'] = summary;
                  }
                  if (typeof image !== typeof undefined && image !== false) {
                        shareData['image'] = image;
                  }
        

                  if(options.template === 'hover') {
                        elem.html('<div class="shareable shareable-hover"><a class="sharenow" href="#"><i class="fa fa-share-alt"></i>Share</a><div class="overlay"><div class="social-box"></div></div></div>');
                  } else if(options.template === 'popup') {
                        var htmlPopup = '<div class="shareable shareable-popup">'+
                        '<a class="sharenow" href="#sharepopup'+count+'"><i class="fa fa-share-alt"></i>Share</a>'+
                        '<div style="display:none">'+
                        '<div id="sharepopup'+count+'" class="popup-container share-popup-block">'+
                        '<h3>Share this</h3>'+
                        '<div class="shareable"><div class="social-box"></div></div>'+
                        '</div>'+
                        '</div>'+
                        '</div>';
                        elem.html(htmlPopup);
                  } else {
                        elem.html('<div class="shareable"><div class="social-box"></div></div>');
                  }

                  buttons = '';
                  var socialBox = elem.find('.social-box');
                  


                  $.map(options.shares, function( network, i ) {
                        options = $.extend({}, $.fn.shareableDefaults, options || {});
                        shareButton = options.providers[network];
                        shareUrlQuery = options.defaults[network];

                 
                        $.map(shareUrlQuery, function( value, key ) {
                              if(keyExists(key, shareData)) {
                                    shareUrlQuery[key] = shareData[key];
                              }
                             

                        });
                       
                        if(network !== 'email') {
                              buttons += new _createSocialButton(network, socialBox, shareButton, shareUrlQuery);
                        } else {
                              buttons += new _createEmailButton(network, socialBox, shareButton, shareUrlQuery);
                        }

                  });
            });

            $("a.sharelink").click(function(e) {
                  e.preventDefault();
                  var url = this.href,
                  w = 500,
                  h = 400,
                  left = screen.width / 2 - w / 2,
                  top = screen.height / 2 - h / 2;
                  window.open(url, "Shareable Saraswati", "toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=" + w + ", height=" + h + ", top=" + top + ", left=" + left)
            });

            $(".shareable-hover a.sharenow").click(function(e) {
                  e.preventDefault();
                  $(this).parent().find('.overlay').toggleClass("active");
            });

            $(".shareable-popup a.sharenow").click(function(e) {
                  e.preventDefault();
                  $(this).parent().find('.overlay').toggleClass("active");
            });
            $('.shareable-popup a.sharenow').magnificPopup({
                  type:'inline',
                  midClick: true 
            });
      };
      /*------------------------------------------
      * Tweetable
      *--------------------------------------------*/
      $.fn.tweetable = function(options) {
            options = $.extend({}, $.fn.shareableDefaults, options || {});
            this.each(function() {
                  var elem = $(this);
                  elem.text();
                  urlShare =  options.providers['twitter'].url, 
                  urlShare += "url=" + encodeURIComponent(options.defaults['twitter'].url), 
                  urlShare += "&text=" + encodeURIComponent(elem.text()), 
                  urlShare += "&via=" + encodeURIComponent(options.defaults['twitter'].via),
                  elem.html($('<a class="tweetable-link" href="' + urlShare + '" ><span class="tweet_block"><i class="fa fa-twitter "></i><span class="tweet_text">tweet</span></span>' + elem.text() + "</a>"))
            });
            $(".tweetable-link").click(function(e) {
                  e.preventDefault();
                  var url = this.href,
                  w = 500,
                  h = 400,
                  left = screen.width / 2 - w / 2,
                  top = screen.height / 2 - h / 2;
                  window.open(url, "Shareable Saraswati", "toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=" + w + ", height=" + h + ", top=" + top + ", left=" + left)
            });
      };
}(window, jQuery));