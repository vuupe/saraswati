/*! Onepage Navigation (onepageNavigation.js ) - v1.0.0 - 2016-04-07
 *  Desc: Onepage navigation
 *  Requires: 
 *  - 
 *  Copyright: (c) 2016 Wawan - wawan@potential.asia
 *  License: MIT
 */


(function($) {
  $.fn.onepageNavigation = function(options) {
    options = $.extend({}, options || {});
    return this.each(function(i) {
      var lastId,elem = $(this),
      links = elem.find('a'),
      scrollItems = links.map(function(){
        var item = $($(this).attr("href"));
        if (item.length) { return item; }
      });

      elem.addClass('scrollable');

      /*clickable */
        $(links).on('mouseleave',function (e) {
        links.parent().removeClass("hover");
      
        e.preventDefault();
     });
      $(links).on('mouseover',function (e) {
        links.parent().removeClass("hover");
        $(this).parent().addClass("hover");
        e.preventDefault();
     });
    
    $(links).on('click',function (e) {
      links.parent().removeClass("hover");
      $(this).parent().addClass("hover");


       var target = this.hash;
       var $target = $(target);

       $('html, body').stop().animate({
         scrollTop: $target.offset().top+5 /* silly hack plus 5 px */
       }, 900, 'swing', function () {
         window.location.hash = target;
       });
       e.preventDefault();
     });

      /* scrollIndex */
      $(window).scroll(function(){
       var fromTop = $(this).scrollTop();

       var cur = scrollItems.map(function(){
         if ($(this).offset().top < fromTop)
           return this;
       });

       cur = cur[cur.length-1];
       var id = cur && cur.length ? cur[0].id : "";

       if (lastId !== id) {
         lastId = id; 
       }  
       links.parent().removeClass("active").end().filter("[href='#"+lastId+"']").parent().addClass("active");
     });
    });
  };
})(jQuery);