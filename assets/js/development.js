
$(document).ready(function () {
	$('.reference .box-header').click(function(){
		var el = $(this), parent = el.closest('.reference');
		if( el.hasClass('active') )
		{
			parent.find('.box-content').slideToggle();
			el.removeClass('active');
		}
		else
		{
			parent.find('.box-content').slideToggle();
			el.addClass('active');
		}
		return false;
	});
}); 
