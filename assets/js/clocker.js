/*! Clocker (clocker.js ) - v1.0.0 - 2016-04-07
 *  Desc: Realtime counter
 *  Requires: 
 *  - Flipclock
 *  Copyright: (c) 2016 Wawan - wawan@potential.asia
 *  License: MIT
 */

 (function($) {
  $.fn.clocker = function(options) {

    options = $.extend({}, $.fn.clocker.defaults, options || {});

    var increment = 0;
    var freezeTime = false;
    var $clocker;

    if ($.isFunction(window.FlipClock)) {
      $clocker = $(".clocker").FlipClock(0, {
        clockFace: "DailyCounter"
      });
    }

    $(".clock-control a").click(function (e) {
      var seconds = parseInt($(this).attr("href").replace("#",""),10);
      $(".clock-control a").removeClass("active");
      $(this).addClass("active");

      if(seconds === 0) {
        totalSeconds = 0;
        freezeTime = false;
        $clocker.setTime(0);
        $clocker.start();
        $(".clock-control").find(".restart").hide();
      } else {
        totalSeconds = seconds;
        freezeTime = true;
        $clocker.setTime(seconds);
        $clocker.stop();
        $(".clock-control").find(".restart").show();
      }


      e.preventDefault();
    });

    return this.each(function() {
      var elem = $(this);
      var _this = this,

      //run code every x milisecond
      interval = setInterval(updateTimer, options.refreshInterval);

      var dataInterval = parseInt(elem.attr('data-interval'));
      var dataRoundup = parseInt(elem.attr('data-roundup'));
      var dataDecimal = parseInt(elem.attr('data-decimal'),10);
      var dataValue = elem.attr('data-value');
      var dataSymbol = elem.attr('data-symbol');
      var dataMeasure = elem.attr('data-measure');
      var dataType = elem.attr('data-type');
      var dataText = elem.attr('data-text');

      if (typeof dataInterval === typeof undefined && dataInterval === false) {
        dataInterval = 100;
      }
      if (typeof dataRoundup === typeof undefined && dataRoundup === false) {
        dataRoundup = false;
      }

      if (typeof dataDecimal === typeof undefined && dataDecimal === false) {
        dataDecimal = 2;
      }

      if (typeof dataValue === typeof undefined && dataValue === false) {
        dataValue = 0;
      }

      if (typeof dataSymbol === typeof undefined && dataSymbol === false) {
        dataSymbol = false;
      }

      if (typeof dataMeasure === typeof undefined && dataMeasure === false) {
        dataMeasure = false;
      }

      if (typeof dataType === typeof undefined && dataType === false) {
        dataType = '';
      }

      if (typeof dataText === typeof undefined && dataText === false) {
        dataText = '';
      }
      
      function updateTimer() {

        if(!freezeTime) {
          increment += options.increment;
        }
        
        var number = myFixed(parseFloat(dataValue,10) * increment, dataDecimal);
        var text = '';
        if(dataSymbol) {
          text +=  "<span class='symbol'>"+dataSymbol+"</span>";
        }
        if(dataRoundup) {
          text +=  "<span class='increment'>"+Math.round(number)+"</span>";
        } else {
          text += "<span class='increment'>"+number+"</span>";
        }
        if(dataMeasure) {
          text +=  "<span class='measure'>"+dataMeasure+"</span>";
        }
        if(dataType) {
          text +=  "<span class='type'>"+dataType+"</span>";
        }
        if(dataText) {
          text +=  "<span class='text'>"+dataText+"</span>";
        }
        $(_this).html( numberWithCommas( text ) );
      }
      function pad( val ) {
        var valString = val + "";
        if( valString.length < 2 ) {
          return "0" + valString;
        } else {
          return valString;
        }
      } 

      function numberWithCommas( x ) {
        if(x / 1000000000 > 1) {
          return numberWithCommas( Math.round(x / 1000000000) ) + " billion";
        } else if(x / 1000000 > 1) {
          return numberWithCommas( Math.round(x / 1000000) ) + " million";
        }
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      }

      function myFixed(x, d) {
        if (!d) return x.toFixed(d); 
        return x.toFixed(d).replace(/\.?0+$/, '');
      }
  });
  }
  $.fn.clocker.defaults = {
    increment: 0.55,
    refreshInterval: 100 //milisecond
};
})(jQuery);


