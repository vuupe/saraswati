/*! Clocker (clocker.js ) - v1.0.0 - 2016-04-07
 *  Desc: Realtime counter
 *  Requires: 
 *  - Flipclock
 *  Copyright: (c) 2016 Wawan - wawan@potential.asia
 *  License: MIT
 */

 (function($) {
  $.fn.clocker = function(options) {

    options = $.extend({}, $.fn.clocker.defaults, options || {});

    var increment = 0;
    var freezeTime = false;
    var $clocker;

    if ($.isFunction(window.FlipClock)) {
      $clocker = $(".clocker").FlipClock(0, {
        clockFace: "DailyCounter"
      });
    }

    $(".clock-control a").click(function (e) {
      var seconds = parseInt($(this).attr("href").replace("#",""),10);
      $(".clock-control a").removeClass("active");
      $(this).addClass("active");

      if(seconds === 0) {
        totalSeconds = 0;
        freezeTime = false;
        $clocker.setTime(0);
        $clocker.start();
        $(".clock-control").find(".restart").hide();
      } else {
        totalSeconds = seconds;
        freezeTime = true;
        $clocker.setTime(seconds);
        $clocker.stop();
        $(".clock-control").find(".restart").show();
      }


      e.preventDefault();
    });

    return this.each(function() {
      var elem = $(this);
      var _this = this,

      //run code every x milisecond
      interval = setInterval(updateTimer, options.refreshInterval);

      var dataInterval = parseInt(elem.attr('data-interval'));
      var dataRoundup = parseInt(elem.attr('data-roundup'));
      var dataDecimal = parseInt(elem.attr('data-decimal'),10);
      var dataValue = elem.attr('data-value');
      var dataSymbol = elem.attr('data-symbol');
      var dataMeasure = elem.attr('data-measure');
      var dataType = elem.attr('data-type');
      var dataText = elem.attr('data-text');

      if (typeof dataInterval === typeof undefined && dataInterval === false) {
        dataInterval = 100;
      }
      if (typeof dataRoundup === typeof undefined && dataRoundup === false) {
        dataRoundup = false;
      }

      if (typeof dataDecimal === typeof undefined && dataDecimal === false) {
        dataDecimal = 2;
      }

      if (typeof dataValue === typeof undefined && dataValue === false) {
        dataValue = 0;
      }

      if (typeof dataSymbol === typeof undefined && dataSymbol === false) {
        dataSymbol = false;
      }

      if (typeof dataMeasure === typeof undefined && dataMeasure === false) {
        dataMeasure = false;
      }

      if (typeof dataType === typeof undefined && dataType === false) {
        dataType = '';
      }

      if (typeof dataText === typeof undefined && dataText === false) {
        dataText = '';
      }
      
      function updateTimer() {

        if(!freezeTime) {
          increment += options.increment;
        }
        
        var number = myFixed(parseFloat(dataValue,10) * increment, dataDecimal);
        var text = '';
        if(dataSymbol) {
          text +=  "<span class='symbol'>"+dataSymbol+"</span>";
        }
        if(dataRoundup) {
          text +=  "<span class='increment'>"+Math.round(number)+"</span>";
        } else {
          text += "<span class='increment'>"+number+"</span>";
        }
        if(dataMeasure) {
          text +=  "<span class='measure'>"+dataMeasure+"</span>";
        }
        if(dataType) {
          text +=  "<span class='type'>"+dataType+"</span>";
        }
        if(dataText) {
          text +=  "<span class='text'>"+dataText+"</span>";
        }
        $(_this).html( numberWithCommas( text ) );
      }
      function pad( val ) {
        var valString = val + "";
        if( valString.length < 2 ) {
          return "0" + valString;
        } else {
          return valString;
        }
      } 

      function numberWithCommas( x ) {
        if(x / 1000000000 > 1) {
          return numberWithCommas( Math.round(x / 1000000000) ) + " billion";
        } else if(x / 1000000 > 1) {
          return numberWithCommas( Math.round(x / 1000000) ) + " million";
        }
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      }

      function myFixed(x, d) {
        if (!d) return x.toFixed(d); 
        return x.toFixed(d).replace(/\.?0+$/, '');
      }
  });
  }
  $.fn.clocker.defaults = {
    increment: 0.55,
    refreshInterval: 100 //milisecond
};
})(jQuery);



/*! Shareable Socials (shareable.js ) - v1.0.1 - 2016-04-07
 *  Desc: Lightweight social shareable
 *  Requires: 
 *	- Font Awesome
 *    - Manific Popup
 *  Copyright: (c) 2016 Wawan - wawan@potential.asia
 *  License: MIT
 */
 
 (function(window, $, undefined) {
      $.fn.shareableDefaults  =  {
            template: 'default',
            apiKey : 'a2afeff9892be7a9c12b84f36fce4d8dc6ca6dda',
            providers: {
                  twitter: {
                        name : "twitter",
                        url :"http://twitter.com/share?",
                        icon: "fa fa-twitter"
                  },
                  facebook: {
                        name : "facebook",
                        url : "https://www.facebook.com/sharer/sharer.php?",
                        icon: "fa fa-facebook"
                  },
                  linkedin: {
                        name : "linkedin",
                        url :"http://www.linkedin.com/shareArticle?",
                        icon: "fa fa-linkedin"
                  },
                  googleplus: {
                        name : "googleplus",
                        url :"https://plus.google.com/share?",
                        icon: "fa fa-google-plus"
                  },
                  reddit: {
                        name : "reddit",
                        url : "http://www.reddit.com/submit?",
                        icon: "fa fa-reddit"
                  },
                  email: {
                        name : "email",
                        url : "mailto:?",
                        icon: "fa fa-envelope"
                  }
            },
            defaults: {
                  twitter: {
                        text: null,
                        url: null,
                        via: 'shareable'
                  },
                  facebook: {
                        u: null
                  },
                  linkedin: {
                        mini: 'true',
                        url: null,
                        title: null,
                        summary: null
                  },
                  googleplus: {
                        url: null
                  },
                  reddit: {
                        url: null,
                        title: null
                  },
                  email: {
                        subject: null,
                        body: null
                  }
            },
      };

      function keyExists(key, search) {
            if (!search || (search.constructor !== Array && search.constructor !== Object)) {
                  return false;
            }
            for (var i = 0; i < search.length; i++) {
                  if (search[i] === key) {
                        return true;
                  }
            }
            return key in search;
      }

      function _createSocialButton(network, parent, provider, defaults) {
            shareUrl = provider.url;
            shareUrl += $.param(defaults);
            var button = '<a href="'+shareUrl+'" class="soslink sharelink '+network+'"><i class="'+provider.icon+'"></i><span class="name">'+provider.name+'</span></a>';
            $(parent).append(button);
      } 

      function _createEmailButton(network, parent, provider, defaults) {
            shareUrl = provider.url;
            shareUrl += $.param(defaults);
            var button = '<a href="'+shareUrl+'" class="soslink '+network+'"><i class="'+provider.icon+'"></i><span class="name">'+provider.name+'</span></a>';
            $(parent).append(button);
      } 

      /* use bitly shorterner
      problem cant return anything from ajax. next move to nodejs to handle convertion
      */
      function shortner(url, apiKey) {
        
            var link = 'http://google.com/lola',
            finalUrl = null;
         
            if (typeof apiKey !== typeof undefined && apiKey !== false) {
               
              $.ajax({
                  type: 'GET',
                  url: 'https://api-ssl.bitly.com/v3/shorten?access_token='+apiKey+'&longUrl='+link,
                  dataType: 'json',
                  success: function(result) {
                        finalUrl = result.data.url;
                      
                        return false;
                  },
                  error: function(jqXHR, textStatus, errorThrown) {
                       
                  }
              });
            }
            
            return finalUrl;
      }


      $.fn.shareableSocials = function(options) {
            options = $.extend({}, $.fn.shareableDefaults, options || {});

            var count = 0;
            var defaultUrl = window.location.href;
            var shortUrl = shortner(defaultUrl, options.apiKey);
            
            this.each(function() {
                  count++;
                  

                  var elem = $(this);
                  var shareData = [];
                  var url = elem.attr('data-url');
                  var title = elem.attr('data-title');
                  var text = elem.attr('data-desc');
                  var summary = elem.attr('data-desc');
                  var image = elem.attr('data-image');
                  

                  if (typeof url !== typeof undefined && url !== false) {
                        shareData['url'] = url;
                        shareData['u'] = url;
                        shortUrl = shortner(url, options.apiKey);
                  } else {
                        shareData['url'] = defaultUrl;
                        shareData['u'] = defaultUrl;
                  }
                  if (typeof title !== typeof undefined && title !== false) {
                        shareData['title'] = title;
                  }
                  if (typeof text !== typeof undefined && text !== false) {
                        shareData['text'] = text+'  '+ shortUrl;
                  }
                  if (typeof summary !== typeof undefined && summary !== false) {
                        shareData['summary'] = summary+'  '+ shortUrl;
                  }
                  if (typeof image !== typeof undefined && image !== false) {
                        shareData['image'] = image;
                  }


                  if(options.template === 'hover') {
                        elem.html('<div class="shareable shareable-hover"><a class="sharenow" href="#"><i class="fa fa-share-alt"></i>Share</a><div class="overlay"><div class="social-box"></div></div></div>');
                  } else if(options.template === 'popup') {
                        var htmlPopup = '<div class="shareable shareable-popup">'+
                        '<a class="sharenow" href="#sharepopup'+count+'"><i class="fa fa-share-alt"></i>Share</a>'+
                        '<div style="display:none">'+
                        '<div id="sharepopup'+count+'" class="popup-container share-popup-block">'+
                        '<h3>Share this</h3>'+
                        '<div class="shareable"><div class="social-box"></div></div>'+
                        '</div>'+
                        '</div>'+
                        '</div>';
                        elem.html(htmlPopup);
                  } else {
                        elem.html('<div class="shareable"><div class="social-box"></div></div>');
                  }

                  buttons = '';
                  var socialBox = elem.find('.social-box');
                  


                  $.map(options.shares, function( network, i ) {

                        options = $.extend({}, $.fn.shareableDefaults, options || {});
                        shareButton = options.providers[network];
                        shareUrlQuery = options.defaults[network];

                        /* check and replace if value on data exist */
                        $.map(shareUrlQuery, function( value, key ) {
                              if(keyExists(key, shareData)) {
                                    shareUrlQuery[key] = shareData[key];
                              }
                              
                        });
                        if(network !== 'email') {
                              buttons += new _createSocialButton(network, socialBox, shareButton, shareUrlQuery);
                        } else {
                              buttons += new _createEmailButton(network, socialBox, shareButton, shareUrlQuery);
                        }

                  });
            });

            $("a.sharelink").click(function(e) {
                  e.preventDefault();
                  var url = this.href,
                  w = 500,
                  h = 400,
                  left = screen.width / 2 - w / 2,
                  top = screen.height / 2 - h / 2;
                  window.open(url, "Shareable Saraswati", "toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=" + w + ", height=" + h + ", top=" + top + ", left=" + left)
            });

            $(".shareable-hover a.sharenow").click(function(e) {
                  e.preventDefault();
                  $(this).parent().find('.overlay').toggleClass("active");
            });

            $(".shareable-popup a.sharenow").click(function(e) {
                  e.preventDefault();
                  $(this).parent().find('.overlay').toggleClass("active");
            });

            $('.shareable-popup a.sharenow').magnificPopup({
                  type:'inline',
                  midClick: true 
            });
      };
      /*------------------------------------------
      * Tweetable
      *--------------------------------------------*/
      $.fn.tweetable = function(options) {
            options = $.extend({}, $.fn.shareableDefaults, options || {});
            this.each(function() {
                  var elem = $(this);
                  elem.text();
                  urlShare =  options.providers['twitter'].url, 
                  urlShare += "url=" + encodeURIComponent(options.defaults['twitter'].url), 
                  urlShare += "&text=" + encodeURIComponent(elem.text()), 
                  urlShare += "&via=" + encodeURIComponent(options.defaults['twitter'].via),
                  elem.html($('<a class="tweetable-link" href="' + urlShare + '" ><span class="tweet_block"><i class="fa fa-twitter "></i><span class="tweet_text">tweet</span></span>' + elem.text() + "</a>"))
            });
            $(".tweetable-link").click(function(e) {
                  e.preventDefault();
                  var url = this.href,
                  w = 500,
                  h = 400,
                  left = screen.width / 2 - w / 2,
                  top = screen.height / 2 - h / 2;
                  window.open(url, "Shareable Saraswati", "toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=" + w + ", height=" + h + ", top=" + top + ", left=" + left)
            });
      };
}(window, jQuery));
/*! Onepage Navigation (onepageNavigation.js ) - v1.0.0 - 2016-04-07
 *  Desc: Onepage navigation
 *  Requires: 
 *  - 
 *  Copyright: (c) 2016 Wawan - wawan@potential.asia
 *  License: MIT
 */



(function($) {
  $.fn.onepageNavigation = function(options) {
    options = $.extend({}, options || {});
    return this.each(function(i) {
      var lastId,elem = $(this),
      links = elem.find('a'),
      scrollItems = links.map(function(){
        var item = $($(this).attr("href"));
        if (item.length) { return item; }
      });

      elem.addClass('scrollable');

      /*clickable */
      $(links).on('click',function (e) {
       var target = this.hash;
       var $target = $(target);
       $('html, body').stop().animate({
         scrollTop: $target.offset().top
       }, 900, 'swing', function () {
         window.location.hash = target;
       });
       e.preventDefault();
     });

      /* scrollIndex */
      $(window).scroll(function(){
       var fromTop = $(this).scrollTop();
       var cur = scrollItems.map(function(){
         if ($(this).offset().top < fromTop)
           return this;
       });
       cur = cur[cur.length-1];
       var id = cur && cur.length ? cur[0].id : "";

       if (lastId !== id) {
         lastId = id; 
       }  
       links.parent().removeClass("active").end().filter("[href='#"+lastId+"']").parent().addClass("active");

     });
    });
  };
})(jQuery);

$(document).ready(function () {
	$('.reference .box-header').click(function(){
		var el = $(this), parent = el.closest('.reference');
		if( el.hasClass('active') )
		{
			parent.find('.box-content').slideToggle();
			el.removeClass('active');
		}
		else
		{
			parent.find('.box-content').slideToggle();
			el.addClass('active');
		}
		return false;
	});
}); 
