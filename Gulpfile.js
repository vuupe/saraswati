var gulp = require('gulp');
var gutil = require('gulp-util');
var gulpif = require('gulp-if');
var autoprefixer = require('gulp-autoprefixer');
var cssmin = require('gulp-cssmin');
var less = require('gulp-less');
var concat = require('gulp-concat');
var plumber = require('gulp-plumber');
var htmlmin = require('gulp-htmlmin');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var buffer = require('vinyl-buffer');
var source = require('vinyl-source-stream');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var stylus = require('gulp-stylus');
var production = process.env.NODE_ENV === 'production';

var config = {
     bowerDir: './bower_components/' ,
    assetDir: './assets/'
}


/*
|--------------------------------------------------------------------------
| Combine all JS libraries into a single file for fewer HTTP requests.
|--------------------------------------------------------------------------
*/
gulp.task('scripts-vendor', function() {
  return gulp.src([
    config.bowerDir +'jquery/dist/jquery.min.js',
    config.bowerDir +'FlipClock/compiled/flipclock.js',
    config.bowerDir +'magnific-popup/dist/jquery.magnific-popup.min.js',
    config.bowerDir +'masonry/dist/masonry.pkgd.min.js',
    config.bowerDir +'imagesloaded/imagesloaded.pkgd.min.js',
    config.assetDir +'vendor/js/iframeResizer.contentWindow.min.js',
    config.bowerDir +'tether/dist/js/tether.js',
    config.bowerDir +'bootstrap/dist/js/bootstrap.js'
  ]).pipe(concat('saraswati.vendor.min.js'))
  .pipe(uglify({ mangle: false }))
  .pipe(gulp.dest('dist/js'));
});

gulp.task('scripts', function() {
  return gulp.src([
    config.assetDir +'js/clocker.js',
    config.assetDir +'js/shareable.js',
    config.assetDir +'js/onepageNavigation.js', 
    config.assetDir +'js/development.js'
  ]).pipe(concat('saraswati.js'))
  .pipe(gulp.dest('dist/js'));
});


/*
|--------------------------------------------------------------------------
| Combine all JS libraries into a single file for fewer HTTP requests.
|--------------------------------------------------------------------------
*/
gulp.task('styles-vendor', function() {
  return gulp.src([
    config.bowerDir +'bootstrap/dist/css/bootstrap.css',
    config.bowerDir +'font-awesome/css/font-awesome.css',
    config.bowerDir +'magnific-popup/dist/magnific-popup.css',
    config.bowerDir +'FlipClock/compiled/flipclock.css',
    config.bowerDir +'tether/dist/css/tether.css',
  ])
  .pipe(concat('saraswati.vendor.min.css'))
  .pipe(stylus())
  .pipe(cssmin())
  .pipe(gulp.dest('dist/css'));
});
gulp.task('styles', function() {
  return gulp.src([
    config.assetDir +'styl/extends.styl',
    config.assetDir +'styl/flaticon.styl',
    config.assetDir +'styl/clocker.styl',
    config.assetDir +'styl/shareable.styl',
    config.assetDir +'styl/onepageNavigation.styl',
    config.assetDir +'styl/development.styl'
  ])
  .pipe(concat('saraswati.css'))
  .pipe(stylus())
  .pipe(gulp.dest('dist/css'));
});

/*
|--------------------------------------------------------------------------
| Combine all JS libraries into a single file for fewer HTTP requests.
|--------------------------------------------------------------------------
*/
gulp.task('fonts', function() { 
  return gulp.src([
    config.bowerDir +'font-awesome/fonts/**/*',
    config.assetDir +'fonts/**/*',
  ]) .pipe(gulp.dest('dist/fonts')); 
});

/*
|--------------------------------------------------------------------------
| Minify images -> convert to webtp
|--------------------------------------------------------------------------
*/
gulp.task('images', function() { 
  return gulp.src([
    config.assetDir +'img/**/*',
  ]) 
  .pipe(gulp.dest('dist/img')); 
});


/*
|--------------------------------------------------------------------------
| Minify images -> convert to webtp
|--------------------------------------------------------------------------
*/
gulp.task('media', function() { 
  return gulp.src([
    config.assetDir +'media/**/*',
  ]) 
  .pipe(imagemin({
            progressive: true,
            svgoPlugins: [
                {removeViewBox: false},
                {cleanupIDs: false}
            ],
            use: [pngquant()]
  }))
  .pipe(gulp.dest('dest/media')); 
});

/*
|--------------------------------------------------------------------------
| Watch JS & CSS
|--------------------------------------------------------------------------
*/
gulp.task('watch', function() {
  gulp.watch(config.assetDir +'js/*.js', ['scripts']);
  gulp.watch(config.assetDir +'styl/*.styl', ['styles']);
});

/*
|--------------------------------------------------------------------------
| Taskers
|--------------------------------------------------------------------------
*/
gulp.task('default', ['images', 'media', 'styles',  'styles-vendor', 'fonts', 'scripts', 'scripts-vendor']);
gulp.task('watchme', ['images', 'media', 'styles',  'styles-vendor', 'fonts', 'scripts', 'scripts-vendor', 'watch']);
