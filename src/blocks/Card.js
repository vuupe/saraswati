import React from 'react';


export default class CardInfographic extends React.Component {
 static get defaultProps() {
    return {
      background: 'none',
      class : 'card-default'
    }
  }
	render() {
	 var style = {
        background: this.props.background
    };
    var elementClass = 'card '+this.props.class;
    return (
    		 <div className={elementClass} style={style}>
              <div className="card-block">
                  {this.props.children}
              </div>
        </div>
    	);
  	}
}