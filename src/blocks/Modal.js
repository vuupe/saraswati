import React from 'react';


export default class Modal extends React.Component {
	static get defaultProps() {
		return {
			id: 'modalId',
			title: false,
		}
	}
	render() {
    var title;

    if(this.props.title) {
      title = (<h4 className="modal-title">{this.props.title}</h4>);
    }
		return(
	<div className="modal fade"  id={this.props.id} data-backdrop="static" data-keyboard="false">
  <div className="modal-dialog">
    <div className="modal-content">
      <div className="modal-header">
      <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" >×</span></button>
        {title}
      </div>
      <div className="modal-body">
            {this.props.children}
      </div>
    </div>
  </div>
</div>
  	)
  }
}