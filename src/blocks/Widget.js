import React from 'react';

export default class Widget extends React.Component {
	static get defaultProps() {
		return {
			elemTag : 'div',
			background: 'none'
		}
	}
	render() {
		var style = {
				background: this.props.background
		};
		let ElemTag = this.props.elemTag;
		let elemId = this.props.id;
		let elemClass = "block  " + this.props.class ? this.props.class : 'block-default';
    	return (
    	<ElemTag  className={elemClass} id={elemId} style={style}>
        		{this.props.children}
        </ElemTag>
    	);
  	}
}