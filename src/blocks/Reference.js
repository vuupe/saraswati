import React from 'react';

export default class Reference extends React.Component {
	render() {
		var data = this.props.data;
		var title = this.props.title;
			return(
				<div className="reference-container">
					<div className="reference">
						<div className="box-header" dangerouslySetInnerHTML={{__html: title}} />
						<div className="box-content">
							{this.props.children}
						</div>
					</div>
				</div>
			)
		}
}