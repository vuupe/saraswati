import React from 'react';
export default class Chart extends React.Component {
	render() {
		var media;
		media = (<img src={this.props.chart_url} />);
		return(
			<div class className="media-wrapper">
				{media}
				<div className="media-share">
					<div className="media-share-inner">
						{this.props.children}
					</div>
				</div>
			</div>
		)
	}
		
}