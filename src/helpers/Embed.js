import React from 'react';


export default class Embed extends React.Component {
	static get defaultProps() {
		return {
			url: "#",
			preview: false,
			iframe : false,
			backlink: false,
			resizer : false	
		}
	}
	constructor(props) {
		super(props);
	}
	render() {
		var embed = "";

		if(this.props.preview) {
			embed += "<a href=\""+this.props.url+"\"> <img src=\""+this.props.preview+"\" /></a>";
		}
		if(this.props.iframe) {
		 	embed += "<iframe src=\""+this.props.iframe+"\" border=\"0\" width=\"100%\"  scrolling=\"no\" style=\"border:none;\"><\/iframe>";
			embed += "<script src=\""+this.props.resizer+"\/public\/assets\/js\/embed.js\"><\/script> <script>iFrameResize()<\/script><br\/> ";
		}

		if(this.props.backlink) {
			 embed += ""+this.props.backlink+"";
		}
		return(
				<div className="embed-box">
					<textarea className="embedcode">{embed}</textarea>
				</div>
			)
		}

	}