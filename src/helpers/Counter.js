import React from 'react';
export default class Counter extends React.Component {
	static get defaultProps() {
		return {
			elemenTag : 'span',
			value: false,
			increment: false,
			roundup: false,
			decimal : false,
			interval: false,
			measure: false,
			symbol : false
		}
	}
	constructor(props) {
		super(props);
	}
	render() {
		let ElemTag = this.props.elemTag;
    	let elemClass = this.props.elemClass ? this.props.elemClass : 'counter-box';
    	var symbol,measure,counter;
    	if(this.props.symbol) {
    		symbol = (<span className="symbol">{this.props.symbol}</span>);		
    	}
    	if(this.props.measure) {
    		measure = (<span className="measure">{this.props.measure}</span>);		
    	}
    	if(this.props.value) {
    		counter = (<span className="counter" data-value={this.props.value} data-roundup={this.props.roundup}  data-decimal={this.props.decimal} data-interval={this.props.interval} data-increment={this.props.increment}>{this.props.value}</span>);
    	} else {
    		counter = (<span className="no-counter">0</span>);
    	}
		return(
			<ElemTag className={elemClass}>{symbol}{counter}{measure}</ElemTag>
			)
		}
	}