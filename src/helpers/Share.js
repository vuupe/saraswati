import React from 'react';

export default class Share extends React.Component {
	static get defaultProps() {
		return {
			elemTag : 'div',
			url: "#"
		}
	}
	constructor(props) {
		super(props);
	}
	render() {
		let ElemTag = this.props.elemTag;
    	let elemClass = this.props.elemClass ? this.props.elemClass : 'share';
		return(
			<ElemTag className={elemClass} data-title={this.props.title} data-desc={this.props.desc} data-image={this.props.image} data-tweet={this.props.tweet} data-subject={this.props.subject} data-body={this.props.body}></ElemTag>	
			)
		}

	}