import React from 'react';

export default class Thumbnail extends React.Component {
	static get defaultProps() {
		return {
			elemTag : 'div',
			elemClass: 'title',
			image: false
		}
	}
	render() {
		let ElemTag = this.props.elemTag;
    	let elemClass = this.props.elemClass ? this.props.elemClass : 'thubnail-box';
		icon = (<img src={this.props.image} />);
		
    	return (
    		 <ElemTag className={elemClass}>{icon}</ElemTag>
    	);
  	}
}