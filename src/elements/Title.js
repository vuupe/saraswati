import React from 'react';


class Title extends React.Component {
	static get defaultProps() {
		return {
			elemTag : 'h3',
			elemClass: 'title',
			icon: false,
			image: false
		}
	}
	constructor(props) {
		super(props);
	}
	render() {
		let ElemTag = this.props.elemTag;
    	let elemClass = this.props.elemClass ? this.props.elemClass : 'title';
		var icon;
		if(this.props.image) {
			icon = (<span className="icon icon-image"><img src={this.props.image} /></span>);
		}
		if(this.props.icon) {
			icon = (<i className={this.props.icon}></i>);
		}
    	return (
    		 <ElemTag className={elemClass}> {icon}<span className="text">{this.props.children}</span></ElemTag>
    	);
  	}
}

export default Title;