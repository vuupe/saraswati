import React from 'react';

export default class Icon extends React.Component {
	static get defaultProps() {
		return {
			elemTag : 'span',
			elemClass: 'title',
			icon: false,
			image: false,
		}
	}

	render() {
		
		let ElemTag = this.props.elemTag;
    	let elemClass = this.props.elemClass ? this.props.elemClass : 'title';
		var icon;

		if(this.props.image) {
			icon = (<span className="icon icon-image"><img src={this.props.image} /></span>);
		}

		if(this.props.icon) {
			icon = (<i className={this.props.icon}></i>);
		}

    	return (
    		 <ElemTag className={elemClass}>{icon}</ElemTag>
    	);
  	}
}