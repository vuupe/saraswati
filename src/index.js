import ParalaxX from './layouts/Paralax';
import SectionX from './layouts/Section';

import CardX from './blocks/Card';
import ModalX from './blocks/Modal';
import ReferenceX from './blocks/Reference';
import WidgetX from './blocks/Widget';

import IconX from './elements/Icon';
import ThumbnailX from './elements/Icon';
import TitleX from './elements/Title';


import ChartX from './helpers/Chart';
import CounterX from './helpers/Counter';
import EmbedX from './helpers/Embed';
import ShareX from './helpers/Share';

export default class Saraswati {



}

/*layout*/
export class Section extends SectionX {};
export class Paralax extends ParalaxX {};

/*Block */
export class Card extends CardX {};
export class Modal extends ModalX {};
export class Reference extends ReferenceX {};
export class Widget extends WidgetX {};

/*elements */
export class Icon extends IconX {};
export class Thumbnail extends ThumbnailX {};
export class Title extends TitleX {};

/*realtime */
export class Chart extends ChartX {};
export class Counter extends CounterX {};
export class Embed extends EmbedX {};
export class Share extends ShareX {};


 