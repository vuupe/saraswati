import React from 'react';


export default class Paralax extends React.Component {
	static get defaultProps() {
		return {
            elemTag : 'section',
			elemClass: 'section-default',
			container: false,
			background: 'none'
		}
	}
	render() {
		var style = {
				background: this.props.background
		};
		let ElemTag = this.props.elemTag;
		var elemId = this.props.id;
		var elemClass = "section  " + this.props.elemClass ? this.props.elemClass : 'section-default';
		var containerClass = this.props.container ? this.props.container : 'container';
    	return (
    	<ElemTag className={elemClass} id={elemId} >
    	    <div className="parallax" style={style}>
                <div className="parallax-overlay"></div>
                <div className={containerClass}>
            		{this.props.children}
                </div>
            </div>
        </ElemTag>
    	);
  	}
}